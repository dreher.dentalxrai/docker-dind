FROM docker:20.10.21-dind as upstream

# copy everything to a clean image, so we can change the exposed ports
# see https://gitlab.com/gitlab-org/gitlab-runner/-/issues/29130#note_1028738753
FROM scratch

COPY --from=upstream / /

VOLUME /var/lib/docker

#EXPOSE 2375/tcp # is for insecure connections, and having both breaks Gitlab's "wait-for-it" service
EXPOSE 2376/tcp

ENTRYPOINT ["dockerd-entrypoint.sh"]
CMD []

ENV DOCKER_VERSION 20.10.21
ENV DOCKER_TLS_CERTDIR=/certs
